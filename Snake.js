class Snake {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.xspeed = 1;
        this.yspeed = 0;
        this.total = 0;
        this.tail = [];
    }


    dir(x, y) {
        this.xspeed = x;
        this.yspeed = y;
    }

    update() {
        for (var i = 0; i < this.tail.length - 1; i++) {
            this.tail[i] = this.tail[i + 1];
          }
          if (this.total >= 1) {
            this.tail[this.total - 1] = createVector(this.x, this.y);
          }

        this.x += this.xspeed * scl;
        this.y += this.yspeed * scl;
        this.x = constrain(this.x, 0, width - scl)
        this.y = constrain(this.y, 0, height - scl)
    }

    show() {
        fill(255)
        for (let i = 0; i < this.tail.length; i++) {
            rect(this.tail[i].x, this.tail[i].y, scl, scl);
        }
        rect(this.x, this.y, scl, scl); 
    }

    eat(poisiton) {
        let d = dist(this.x, this.y, poisiton.x, poisiton.y);
        if (d < 1) {
            this.total++;
            return true;
        }
        else {
            return false;
        }
    }

    death() {
        for (let i = 0; i < this.tail.length; i++) {
            var pos = this.tail[i];
            var d = dist(this.x, this.y, pos.x, pos.y);
            if (d < 1) {
                let score = (this.total + 1) * 100;
                this.total = 0;
                this.tail = []
                alert("Game Over! | Score: " + score);
            }

        }
    }
    
}