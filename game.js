var s;
var scl = 20;
var food;
var vCanvas;

function setup() {
    vCanvas = createCanvas(600, 600);
    vCanvas.parent('gameArea');
    s = new Snake();
    frameRate(10);
    food = new Food();
    food.pickLocation();
}



function draw() {
    background(51);
    if (s.eat(food.pos)) {
        food.pickLocation();
    }
    food.show();
    s.death();
    s.update();
    s.show();
    
    
}

function mousePressed() {
    if (mouseX < width/2 && mouseY < height/2) {
        snakeUp();
    } else if (mouseX > width/2 && mouseY < height/2) {
        snakeDown();
    } else if (mouseX > width/2 && mouseY > height/2) {
        snakeRight();
    } else if (mouseX < width/2 && mouseY > height/2) {
        snakeLeft();
    }
}

function keyPressed() {
    if (keyCode == UP_ARROW || keyCode == 87) {
        snakeUp();
    } else if (keyCode == DOWN_ARROW || keyCode == 83) {
        snakeDown();
    } else if (keyCode == RIGHT_ARROW || keyCode == 68) {
        snakeRight();
    } else if (keyCode == LEFT_ARROW || keyCode == 65) {
        snakeLeft();
    }
}

function snakeUp() {
    s.dir(0, -1);
}

function snakeDown() {
    s.dir(0, 1);
}

function snakeLeft() {
    s.dir(-1, 0);
}

function snakeRight() {
    s.dir(1, 0);
}